#!/bin/bash
set -e

set -x

CONFDFE=/data/dalmatinerfe/etc/dalmatinerfe.conf

if [ -z "$DB_NODE" ]; then
    DB_NODE=127.0.0.1
fi

if [ -z "$PG_NODE" ]; then
    PG_NODE=127.0.0.1
fi

sed -i \
    -e "s/^ddb_connection.backend_host = .*/ddb_connection.backend_host = ${DB_NODE}/" \
    -e "s/^idx.pg.backend_host = .*/idx.pg.backend_host = ${PG_NODE}/" \
    $CONFDFE

LOGDFE=/data/dalmatinerfe/log/console.log
> $LOGDFE
/dalmatinerfe/bin/dalmatinerfe start
tail -n 1024 -f $LOGDFE
